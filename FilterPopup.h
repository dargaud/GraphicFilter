///////////////////////////////////////////////////////////////////////////////
// MODULE: FilterPopup
// PURPOSE: Displays a prompt message for the parameters of a custom graphic filter
//          There are 4 kinds of filters:
//          1x1: acts on each pixel based on its previous value
//          3x3: acts on each pixel based on the values of its 9 neighbors
//          5x5: acts on each pixel based on the values of its 25 neighbors
//          1C:  acts on each color of each pixel based on the other colors
// NOTE: you need the files CustomFiler.uir and CustomFilter.h in your CVI project
//       This is LabWindows specific code and needs GraphicFilter.h/c
///////////////////////////////////////////////////////////////////////////////

#ifndef _FILTER_POPUP
#define _FILTER_POPUP

#include "GraphicFilter.h"

extern int Filter1x1Popup(tFilter1x1 *Filter);
extern int Filter3x3Popup(tFilter3x3 *Filter);
extern int Filter5x5Popup(tFilter5x5 *Filter);
extern int Filter1CPopup (tFilter1C  *Filter, BOOL EnableAlpha);

#endif
