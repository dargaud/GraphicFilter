/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PFIL1                            1
#define  PFIL1_NAME                       2       /* control type: string, callback function: cb_Filter1x1Help */
#define  PFIL1_MUL                        3       /* control type: numeric, callback function: cb_Filter1x1Help */
#define  PFIL1_DIV                        4       /* control type: numeric, callback function: cb_Filter1x1Help */
#define  PFIL1_BIAS                       5       /* control type: numeric, callback function: cb_Filter1x1Help */
#define  PFIL1_CANCEL                     6       /* control type: command, callback function: cb_FilterCancel */
#define  PFIL1_OK                         7       /* control type: command, callback function: cb_Filter1x1OK */
#define  PFIL1_TEXTMSG                    8       /* control type: textMsg, callback function: (none) */

#define  PFIL3                            2
#define  PFIL3_NAME                       2       /* control type: string, callback function: cb_Filter3x3Help */
#define  PFIL3_MAT_UL                     3       /* control type: numeric, callback function: cb_Filter3x3Help */
#define  PFIL3_MAT_U                      4       /* control type: numeric, callback function: cb_Filter3x3Help */
#define  PFIL3_MAT_UR                     5       /* control type: numeric, callback function: cb_Filter3x3Help */
#define  PFIL3_MAT_L                      6       /* control type: numeric, callback function: cb_Filter3x3Help */
#define  PFIL3_MAT_C                      7       /* control type: numeric, callback function: cb_Filter3x3Help */
#define  PFIL3_MAT_R                      8       /* control type: numeric, callback function: cb_Filter3x3Help */
#define  PFIL3_MAT_DL                     9       /* control type: numeric, callback function: cb_Filter3x3Help */
#define  PFIL3_MAT_D                      10      /* control type: numeric, callback function: cb_Filter3x3Help */
#define  PFIL3_MAT_DR                     11      /* control type: numeric, callback function: cb_Filter3x3Help */
#define  PFIL3_DIV                        12      /* control type: numeric, callback function: cb_Filter3x3Help */
#define  PFIL3_BIAS                       13      /* control type: numeric, callback function: cb_Filter3x3Help */
#define  PFIL3_CANCEL                     14      /* control type: command, callback function: cb_FilterCancel */
#define  PFIL3_OK                         15      /* control type: command, callback function: cb_Filter3x3OK */

#define  PFIL5                            3
#define  PFIL5_NAME                       2       /* control type: string, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_UULL                   3       /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_UUL                    4       /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_UUC                    5       /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_UUR                    6       /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_UURR                   7       /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_ULL                    8       /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_UL                     9       /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_UC                     10      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_UR                     11      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_URR                    12      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_CLL                    13      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_CL                     14      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_CC                     15      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_CR                     16      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_CRR                    17      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_DLL                    18      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_DL                     19      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_DC                     20      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_DR                     21      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_DRR                    22      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_DDLL                   23      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_DDL                    24      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_DDC                    25      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_DDR                    26      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_MAT_DDRR                   27      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_DIV                        28      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_BIAS                       29      /* control type: numeric, callback function: cb_Filter5x5Help */
#define  PFIL5_CANCEL                     30      /* control type: command, callback function: cb_FilterCancel */
#define  PFIL5_OK                         31      /* control type: command, callback function: cb_Filter5x5OK */

#define  PFILC                            4
#define  PFILC_NAME                       2       /* control type: string, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_RR                     3       /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_RG                     4       /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_RB                     5       /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_RA                     6       /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_DIV_R                      7       /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_BIAS_R                     8       /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_GR                     9       /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_GG                     10      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_GB                     11      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_GA                     12      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_DIV_G                      13      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_BIAS_G                     14      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_BR                     15      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_BG                     16      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_BB                     17      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_BA                     18      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_DIV_B                      19      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_BIAS_B                     20      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_AR                     21      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_AG                     22      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_AB                     23      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_MAT_AA                     24      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_DIV_A                      25      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_BIAS_A                     26      /* control type: numeric, callback function: cb_Filter1CHelp */
#define  PFILC_CANCEL                     27      /* control type: command, callback function: cb_FilterCancel */
#define  PFILC_OK                         28      /* control type: command, callback function: cb_Filter1COK */
#define  PFILC_TEXTMSG                    29      /* control type: textMsg, callback function: (none) */

#define  TEST                             5       /* callback function: cbp_Test */
#define  TEST_LOAD                        2       /* control type: command, callback function: cb_Load */
#define  TEST_1X1                         3       /* control type: command, callback function: cb_FilterPopup */
#define  TEST_3X3                         4       /* control type: command, callback function: cb_FilterPopup */
#define  TEST_5X5                         5       /* control type: command, callback function: cb_FilterPopup */
#define  TEST_CHAN                        6       /* control type: command, callback function: cb_FilterPopup */
#define  TEST_PICTURE                     7       /* control type: picture, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK cb_Filter1CHelp(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Filter1COK(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Filter1x1Help(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Filter1x1OK(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Filter3x3Help(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Filter3x3OK(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Filter5x5Help(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Filter5x5OK(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_FilterCancel(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_FilterPopup(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Load(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_Test(int panel, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
