#include <stdlib.h>
#include <cvirte.h>		
#include <userint.h>
#include "FilterPopup.h"
#include "CustomFilter.h"

static int Ptest;
static int Bitmap=0;

int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((Ptest = LoadPanel (0, "CustomFilter.uir", TEST)) < 0)
		return -1;
	cbp_Test(Ptest, EVENT_PANEL_SIZE, 0, 0, 0);
	DisplayPanel (Ptest);
	RunUserInterface ();
	DiscardPanel (Ptest);
	return 0;
}

int CVICALLBACK cbp_Test (int panel, int event, void *callbackData,
						  int eventData1, int eventData2) {
	int Height, Top, Width;
	switch (event) {
		case EVENT_CLOSE:
			QuitUserInterface (0);
			break;
		case EVENT_PANEL_SIZE:
			GetPanelAttribute(panel, ATTR_HEIGHT, &Height);
			GetPanelAttribute(panel, ATTR_WIDTH, &Width);
			GetCtrlAttribute (panel, TEST_PICTURE, ATTR_TOP, &Top);
			SetCtrlAttribute (panel, TEST_PICTURE, ATTR_HEIGHT, Height-Top);
			SetCtrlAttribute (panel, TEST_PICTURE, ATTR_WIDTH, Width);
			break;
	}
	return 0;
}

int CVICALLBACK cb_Load (int panel, int control, int event,
						 void *callbackData, int eventData1, int eventData2) {
	char Filename[MAX_PATHNAME_LEN];
	switch (event) {
		case EVENT_COMMIT:
			if (VAL_EXISTING_FILE_SELECTED!=FileSelectPopup ("", "*.jpg",
															 "*.tif;*.pcx;*.bmp;*.dib;*.rle;*.ico;(*.jpg;*.jpeg);*.png;*.wmf;*.emf.",
															 "", VAL_LOAD_BUTTON, 0, 0, 1, 0, Filename))
				break;
			if (Bitmap) DiscardBitmap (Bitmap); Bitmap=0;
			GetBitmapFromFile (Filename, &Bitmap);
			SetCtrlBitmap   (panel, TEST_PICTURE, 0, Bitmap);
			SetCtrlAttribute(panel, TEST_PICTURE, ATTR_DIMMED, 0);
			SetCtrlAttribute(panel, TEST_1X1,     ATTR_DIMMED, 0);
			SetCtrlAttribute(panel, TEST_3X3,     ATTR_DIMMED, 0);
			SetCtrlAttribute(panel, TEST_5X5,     ATTR_DIMMED, 0);
			SetCtrlAttribute(panel, TEST_CHAN,    ATTR_DIMMED, 0);
			break;
	}
	return 0;
}

int CVICALLBACK cb_FilterPopup (int panel, int control, int event,
						void *callbackData, int eventData1, int eventData2) {
	static tFilter1x1 Filter1={0};
	static tFilter3x3 Filter3={0};
	static tFilter5x5 Filter5={0};
	static tFilter1C  FilterC={0};
	int Size, RowBytes, Depth, Width, Height, Colors;
	unsigned char *Bits;
	switch (event) {
		case EVENT_COMMIT:
			if (Filter1.Div==0){Filter1=FilterId1x1;	// 1st call
								Filter3=FilterId3x3;
								Filter5=FilterId5x5;
								FilterC=FilterId1C;  }

			switch (control) {
				case TEST_1X1:	if (Filter1x1Popup(&Filter1)) return 0; break;
				case TEST_3X3:	if (Filter3x3Popup(&Filter3)) return 0; break;
				case TEST_5X5:	if (Filter5x5Popup(&Filter5)) return 0; break;
				case TEST_CHAN:	if (Filter1CPopup (&FilterC, 0)) return 0; break;
				default: return 0;
			}
			GetBitmapInfoEx (Bitmap, &Colors, &Size, NULL, NULL);
			if (Colors!=0) return 0;	// not a 24-bit or 32-bit color image
			Bits=malloc(Size);
			GetBitmapDataEx (Bitmap, &RowBytes, &Depth, &Width, &Height, NULL, Bits, NULL, NULL);
			
			switch (control) {
				case TEST_1X1:	Filter1x1(Bits, RowBytes, Depth, Width, Height, &Filter1); break;
				case TEST_3X3:	Filter3x3(Bits, RowBytes, Depth, Width, Height, &Filter3); break;
				case TEST_5X5:	Filter5x5(Bits, RowBytes, Depth, Width, Height, &Filter5); break;
				case TEST_CHAN:	Filter1C (Bits, RowBytes, Depth, Width, Height, &FilterC); break;
				default: return 0;
			}
			
			SetBitmapDataEx (Bitmap, RowBytes, Depth, NULL, Bits, NULL, NULL);
			SetCtrlBitmap (panel, TEST_PICTURE, 0, Bitmap);
			free(Bits);
			break;
	}
	return 0;
}

