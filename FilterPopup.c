///////////////////////////////////////////////////////////////////////////////
// MODULE: FilterPopup
// COPYRIGHT: Guillaume Dargaud 2001-2002 - Free Use and distribution              //
// PURPOSE: Displays a prompt message for the parameters of a custom graphic filter
//          There are 4 kinds of filters:
//          1x1: acts on each pixel based on its previous value
//          3x3: acts on each pixel based on the values of its 9 neighbors
//          5x5: acts on each pixel based on the values of its 25 neighbors
//          1C:  acts on each color of each pixel based on the other colors
// TUTORIAL:  http://www.gdargaud.net/Hack/SourceCode.html                         //
// NOTE: you need the files CustomFiler.uir and CustomFilter.h in your CVI project
//       This is LabWindows specific code and needs GraphicFilter.h/c
///////////////////////////////////////////////////////////////////////////////

#include <userint.h>

#include "Def.h"

#include "GraphicFilter.h"
#include "CustomFilter.h"

#include "FilterPopup.h"

static int PnlFilter1=0, PnlFilter3=0, PnlFilter5=0, PnlFilterC=0;

static tFilter1x1 *Filter1P=NULL;
static tFilter3x3 *Filter3P=NULL;
static tFilter5x5 *Filter5P=NULL;
static tFilter1C  *FilterCP=NULL;

///////////////////////////////////////////////////////////////////////////////
// FUNCTION: Filter1x1Popup
// PURPOSE: Displays a prompt message for the parameters of a custom simple graphic filter
// INOUT: Simple pixel Filter (passed by reference), must be already defined
// RETURN: 0 if success or
//         Negative values indicate an error. See QuitUserInterface
///////////////////////////////////////////////////////////////////////////////
int Filter1x1Popup(tFilter1x1 *Filter) {
	int Res=0;
	
	if (PnlFilter1==0 and 
		(PnlFilter1 = LoadPanel (0, "CustomFilter.uir", PFIL1)) < 0)	// We load it only the first time
		return PnlFilter1;
	
	if (Filter==NULL) return -1;
	Filter1P=Filter;
	
	SetCtrlVal(PnlFilter1, PFIL1_NAME, Filter->Name);
	SetCtrlVal(PnlFilter1, PFIL1_MUL,  Filter->Mult);
	SetCtrlVal(PnlFilter1, PFIL1_DIV,  Filter->Div);
	SetCtrlVal(PnlFilter1, PFIL1_BIAS, Filter->Bias);
	
	InstallPopup(PnlFilter1);
	Res = RunUserInterface();
	RemovePopup (0);

	HidePanel(PnlFilter1);
	return Res;
}

///////////////////////////////////////////////////////////////////////////////
// FUNCTION: Filter3x3Popup
// PURPOSE: Displays a prompt message for the parameters of a custom 3x3 graphic filter
// INOUT: 3x3 Filter (passed by reference), must be already defined
// RETURN: 0 if success or
//         Negative values indicate an error. See QuitUserInterface
///////////////////////////////////////////////////////////////////////////////
int Filter3x3Popup(tFilter3x3 *Filter) {
	int Res=0;
	
	if (PnlFilter3==0 and 
		(PnlFilter3 = LoadPanel (0, "CustomFilter.uir", PFIL3)) < 0)	// We load it only the first time
		return PnlFilter3;
	
	if (Filter==NULL) return -1;
	Filter3P=Filter;
	
	SetCtrlVal(PnlFilter3, PFIL3_NAME, Filter->Name);
	SetCtrlVal(PnlFilter3, PFIL3_DIV, Filter->Div);
	SetCtrlVal(PnlFilter3, PFIL3_BIAS, Filter->Bias);
	
	SetCtrlVal(PnlFilter3, PFIL3_MAT_UL, Filter->Mult[0][0]);
	SetCtrlVal(PnlFilter3, PFIL3_MAT_L,  Filter->Mult[1][0]);
	SetCtrlVal(PnlFilter3, PFIL3_MAT_DL, Filter->Mult[2][0]);

	SetCtrlVal(PnlFilter3, PFIL3_MAT_U,  Filter->Mult[0][1]);
	SetCtrlVal(PnlFilter3, PFIL3_MAT_C,  Filter->Mult[1][1]);
	SetCtrlVal(PnlFilter3, PFIL3_MAT_D,  Filter->Mult[2][1]);
	
	SetCtrlVal(PnlFilter3, PFIL3_MAT_UR, Filter->Mult[0][2]);
	SetCtrlVal(PnlFilter3, PFIL3_MAT_R,  Filter->Mult[1][2]);
	SetCtrlVal(PnlFilter3, PFIL3_MAT_DR, Filter->Mult[2][2]);
	
	InstallPopup(PnlFilter3);
	Res = RunUserInterface();
	RemovePopup (0);

	HidePanel(PnlFilter3);
	return Res;
}

///////////////////////////////////////////////////////////////////////////////
// FUNCTION: FilterPopup
// PURPOSE: Displays a prompt message for the parameters of a custom 5x5 graphic filter
// INOUT: 5x5 Filter (passed by reference), must be already defined
// RETURN: 0 if success or
//         Negative values indicate an error. See QuitUserInterface
///////////////////////////////////////////////////////////////////////////////
int Filter5x5Popup(tFilter5x5 *Filter) {
	int Res=0;
	
	if (PnlFilter5==0 and 
		(PnlFilter5 = LoadPanel (0, "CustomFilter.uir", PFIL5)) < 0)	// We load it only the first time
		return PnlFilter5;
	
	if (Filter==NULL) return -1;
	Filter5P=Filter;
	
	SetCtrlVal(PnlFilter5, PFIL5_NAME, Filter->Name);
	SetCtrlVal(PnlFilter5, PFIL5_DIV,  Filter->Div);
	SetCtrlVal(PnlFilter5, PFIL5_BIAS, Filter->Bias);
	
	SetCtrlVal(PnlFilter5, PFIL5_MAT_UULL, Filter->Mult[0][0]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_ULL,  Filter->Mult[1][0]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_CLL,  Filter->Mult[2][0]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_DLL,  Filter->Mult[3][0]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_DDLL, Filter->Mult[4][0]);

	SetCtrlVal(PnlFilter5, PFIL5_MAT_UUL,  Filter->Mult[0][1]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_UL,   Filter->Mult[1][1]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_CL,   Filter->Mult[2][1]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_DL,   Filter->Mult[3][1]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_DDL,  Filter->Mult[4][1]);

	SetCtrlVal(PnlFilter5, PFIL5_MAT_UUC,  Filter->Mult[0][2]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_UC,   Filter->Mult[1][2]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_CC,   Filter->Mult[2][2]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_DC,   Filter->Mult[3][2]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_DDC,  Filter->Mult[4][2]);

	SetCtrlVal(PnlFilter5, PFIL5_MAT_UUR,  Filter->Mult[0][3]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_UR,   Filter->Mult[1][3]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_CR,   Filter->Mult[2][3]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_DR,   Filter->Mult[3][3]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_DDR,  Filter->Mult[4][3]);

	SetCtrlVal(PnlFilter5, PFIL5_MAT_UURR, Filter->Mult[0][4]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_URR,  Filter->Mult[1][4]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_CRR,  Filter->Mult[2][4]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_DRR,  Filter->Mult[3][4]);
	SetCtrlVal(PnlFilter5, PFIL5_MAT_DDRR, Filter->Mult[4][4]);

	InstallPopup(PnlFilter5);
	Res = RunUserInterface();
	RemovePopup (0);

	HidePanel(PnlFilter5);
	return Res;
}



///////////////////////////////////////////////////////////////////////////////
// FUNCTION: Filter1CPopup
// PURPOSE: Displays a prompt message for the parameters of a custom interchannel graphic filter
// INOUT: Interchannel Filter (passed by reference), must be already defined
// RETURN: 0 if success or
//         Negative values indicate an error. See QuitUserInterface
///////////////////////////////////////////////////////////////////////////////
int Filter1CPopup(tFilter1C *Filter, BOOL EnableAlpha) {
	int Res=0;
	
	if (PnlFilterC==0 and 
		(PnlFilterC = LoadPanel (0, "CustomFilter.uir", PFILC)) < 0)	// We load it only the first time
		return PnlFilterC;
	
	if (Filter==NULL) return -1;
	FilterCP=Filter;

	SetCtrlAttribute(PnlFilterC, PFILC_MAT_RA,  ATTR_DIMMED, !EnableAlpha);
	SetCtrlAttribute(PnlFilterC, PFILC_MAT_GA,  ATTR_DIMMED, !EnableAlpha);
	SetCtrlAttribute(PnlFilterC, PFILC_MAT_BA,  ATTR_DIMMED, !EnableAlpha);
	SetCtrlAttribute(PnlFilterC, PFILC_MAT_AR,  ATTR_DIMMED, !EnableAlpha);
	SetCtrlAttribute(PnlFilterC, PFILC_MAT_AG,  ATTR_DIMMED, !EnableAlpha);
	SetCtrlAttribute(PnlFilterC, PFILC_MAT_AB,  ATTR_DIMMED, !EnableAlpha);
	SetCtrlAttribute(PnlFilterC, PFILC_MAT_AA,  ATTR_DIMMED, !EnableAlpha);
	SetCtrlAttribute(PnlFilterC, PFILC_DIV_A,  	ATTR_DIMMED, !EnableAlpha);
	SetCtrlAttribute(PnlFilterC, PFILC_BIAS_A,	ATTR_DIMMED, !EnableAlpha);

	
	SetCtrlVal(PnlFilterC, PFILC_NAME, Filter->Name);

	SetCtrlVal(PnlFilterC, PFILC_MAT_RR,  Filter->Mult[0][0]);
	SetCtrlVal(PnlFilterC, PFILC_MAT_RG,  Filter->Mult[0][1]);
	SetCtrlVal(PnlFilterC, PFILC_MAT_RB,  Filter->Mult[0][2]);
	SetCtrlVal(PnlFilterC, PFILC_MAT_RA,  Filter->Mult[0][3]);
	
	SetCtrlVal(PnlFilterC, PFILC_MAT_GR,  Filter->Mult[1][0]);
	SetCtrlVal(PnlFilterC, PFILC_MAT_GG,  Filter->Mult[1][1]);
	SetCtrlVal(PnlFilterC, PFILC_MAT_GB,  Filter->Mult[1][2]);
	SetCtrlVal(PnlFilterC, PFILC_MAT_GA,  Filter->Mult[1][3]);

	SetCtrlVal(PnlFilterC, PFILC_MAT_BR,  Filter->Mult[2][0]);
	SetCtrlVal(PnlFilterC, PFILC_MAT_BG,  Filter->Mult[2][1]);
	SetCtrlVal(PnlFilterC, PFILC_MAT_BB,  Filter->Mult[2][2]);
	SetCtrlVal(PnlFilterC, PFILC_MAT_BA,  Filter->Mult[2][3]);

	SetCtrlVal(PnlFilterC, PFILC_MAT_AR,  Filter->Mult[3][0]);
	SetCtrlVal(PnlFilterC, PFILC_MAT_AG,  Filter->Mult[3][1]);
	SetCtrlVal(PnlFilterC, PFILC_MAT_AB,  Filter->Mult[3][2]);
	SetCtrlVal(PnlFilterC, PFILC_MAT_AA,  Filter->Mult[3][3]);

	SetCtrlVal(PnlFilterC, PFILC_DIV_R,  Filter->Div[0]);
	SetCtrlVal(PnlFilterC, PFILC_DIV_G,  Filter->Div[1]);
	SetCtrlVal(PnlFilterC, PFILC_DIV_B,  Filter->Div[2]);
	SetCtrlVal(PnlFilterC, PFILC_DIV_A,  Filter->Div[3]);
	
	SetCtrlVal(PnlFilterC, PFILC_BIAS_R, Filter->Bias[0]);
	SetCtrlVal(PnlFilterC, PFILC_BIAS_G, Filter->Bias[1]);
	SetCtrlVal(PnlFilterC, PFILC_BIAS_B, Filter->Bias[2]);
	SetCtrlVal(PnlFilterC, PFILC_BIAS_A, Filter->Bias[3]);
	
	InstallPopup(PnlFilterC);
	
	Res = RunUserInterface();
	RemovePopup (0);

	HidePanel(PnlFilterC);
	return Res;
}


///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Filter1x1Help (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:

			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Custom 3x3 Filter Help",
			"A custom filter takes the values of a pixel and its 8 surrounding pixels\n"
			"and computes a new color based on some parameters.\n"
			"The global formula is:\n"
			"NewPixelValue = ( OldPixelValue * Mult ) / Div + Bias\n"
			"\nExamples: (M D B)\n"
			" 1 1    0  Changes nothing\n"
			"-1 1  255  Negative\n"
			" 1 1   32  Lighter\n"
			" 2 1 -128 Contrast More\n"
			"\nWhen you write a filter bear in mind that:\n"
			"- Div must be >0\n"
			"- The values of all parameters should be better kept between -128 and 127.\n"
			"- The Bias should be >0 for a lighter image but more generally...\n"
			"- if you want the average brightness to stay constant\n"
			"  you should have the Mult/Div + Bias = 1\n"
			"- 1x1 filters are equivalent to 3x3 filters with all Matrix values to 0 except the center one which is Mult.\n"
			"\nPlay around with it, it's very powerful to enhance pictures. All the predefined filters use this method.");

			break;
	}
	return 0;
}

int CVICALLBACK cb_Filter3x3Help (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:

			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Custom Filter Help",
			"A custom filter takes the values of a pixel and computes a new color based on some parameters.\n"
			"The global formula is:\n"
			"NewPixelCC=\n"
			"  (MatUL*PosUL + MatUC*PosUC + MatUR*PosUR +\n"
			"   MatCL*PosCL + MatCC*PosCC + MatCR*PosCR +\n"
			"   MatDL*PosDL + MatDC*PosDC + MatDR*PosDR )\n"
			"  / Div + Bias\n"
			"Where L is the left pixel, U the upper pixel, C the center one...\n"
			"\nExamples:\n"
			"0 0 0  Changes nothing\n"
			"0 1 0  Div=1 Bias=0\n"
			"0 0 0  \n\n"
			"0 0 0  Contrast More\n"
			"0 2 0  Div=1 Bias=-128\n"
			"0 0 0  \n\n"
			"-1 -1 -1  Sharpen\n"
			"-1 16 -1  Div=8 Bias=0\n"
			"-1 -1 -1  \n\n"
			"When you write a filter bear in mind that:\n"
			"- Div must be >0\n"
			"- MatCC is the current pixel value, so it's often weighted more.\n"
			"- The values of all parameters should be better kept between -128 and 127.\n"
			"- The Bias should be >0 for a lighter image but more generally...\n"
			"- if you want the average brightness to stay constant\n"
			"  you should have the (sum of matrix coeficiant)/Div + Bias = 1\n"
			"- if you only want to act on colors, put all Mat to 0 except the center one.\n"
			"\nPlay around with it, it's very powerful to enhance pictures. All the predefined filters use this method.");

			break;
	}
	return 0;
}

int CVICALLBACK cb_Filter5x5Help (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:

			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Custom 5x5 Filter Help",
			"This is very similar to 3x3 filters but it uses the 25 surrounding pixels.");

			break;
	}
	return 0;
}

int CVICALLBACK cb_Filter1CHelp (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:

			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Custom Inter-Channel Filter Help",
			"This filter allows you to mix the color channels of each pixel.\n"
			"This filter works only on RGB or RGBA images (24 or 32 bits).\n"
			"Each new color is computed this way:\n"
			"R = ( Mrr.R+Mrg.G+Mrb.B+Mra.A ) / Dr + Br\n"
			"G = ( Mgr.R+Mgg.G+Mgb.B+Mga.A ) / Dg + Bg\n"
			"B = ( Mbr.R+Mbg.G+Mbb.B+Mba.A ) / Db + Bb\n"
			"A = ( Mar.R+Mag.G+Mab.B+Maa.A ) / Da + Ba\n"
			"(If the image is 24 bits, ignore anything with A in it)\n"
			"Example: \n"
			"Mrr=0, Mrg=1, Mrb=0, Dr=1, Br=0\n"
			"Mrr=0, Mrg=0, Mrb=1, Dr=1, Br=0\n"
			"Mrr=1, Mrg=0, Mrb=0, Dr=1, Br=0\n"
			"Will transform Green to red, Blue to Green and Red to Blue\n\n"
			"Mrr=1, Mrg=1, Mrb=1, Dr=3, Br=0\n"
			"Mrr=1, Mrg=1, Mrb=1, Dr=3, Br=0\n"
			"Mrr=1, Mrg=1, Mrb=1, Dr=3, Br=0\n"
			"Will produce a greyscale image from a color one."
			);
			break;
	}
	return 0;
}

int CVICALLBACK cb_Filter1x1OK (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlFilter1, PFIL1_NAME,  Filter1P->Name);
			GetCtrlVal(PnlFilter1, PFIL1_MUL,  &Filter1P->Mult);
			GetCtrlVal(PnlFilter1, PFIL1_DIV,  &Filter1P->Div);
			GetCtrlVal(PnlFilter1, PFIL1_BIAS, &Filter1P->Bias);

			QuitUserInterface(0);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Confirm Custom Filter values",
				"Apply the custom values of your filter.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Filter3x3OK (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlFilter3, PFIL3_NAME,  Filter3P->Name);
			GetCtrlVal(PnlFilter3, PFIL3_DIV,  &Filter3P->Div);
			GetCtrlVal(PnlFilter3, PFIL3_BIAS, &Filter3P->Bias);
	
			GetCtrlVal(PnlFilter3, PFIL3_MAT_UL, &Filter3P->Mult[0][0]);
			GetCtrlVal(PnlFilter3, PFIL3_MAT_L,  &Filter3P->Mult[1][0]);
			GetCtrlVal(PnlFilter3, PFIL3_MAT_DL, &Filter3P->Mult[2][0]);

			GetCtrlVal(PnlFilter3, PFIL3_MAT_U,  &Filter3P->Mult[0][1]);
			GetCtrlVal(PnlFilter3, PFIL3_MAT_C,  &Filter3P->Mult[1][1]);
			GetCtrlVal(PnlFilter3, PFIL3_MAT_D,  &Filter3P->Mult[2][1]);
	
			GetCtrlVal(PnlFilter3, PFIL3_MAT_UR, &Filter3P->Mult[0][2]);
			GetCtrlVal(PnlFilter3, PFIL3_MAT_R,  &Filter3P->Mult[1][2]);
			GetCtrlVal(PnlFilter3, PFIL3_MAT_DR, &Filter3P->Mult[2][2]);

			QuitUserInterface(0);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Confirm Custom Filter values",
				"Apply the custom values of your filter.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Filter5x5OK (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlFilter5, PFIL5_NAME,  Filter5P->Name);
			GetCtrlVal(PnlFilter5, PFIL5_DIV,  &Filter5P->Div);
			GetCtrlVal(PnlFilter5, PFIL5_BIAS, &Filter5P->Bias);
	
			GetCtrlVal(PnlFilter5, PFIL5_MAT_UULL, &Filter5P->Mult[0][0]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_ULL,  &Filter5P->Mult[1][0]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_CLL,  &Filter5P->Mult[2][0]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_DLL,  &Filter5P->Mult[3][0]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_DDLL, &Filter5P->Mult[4][0]);

			GetCtrlVal(PnlFilter5, PFIL5_MAT_UUL,  &Filter5P->Mult[0][1]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_UL,   &Filter5P->Mult[1][1]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_CL,   &Filter5P->Mult[2][1]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_DL,   &Filter5P->Mult[3][1]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_DDL,  &Filter5P->Mult[4][1]);

			GetCtrlVal(PnlFilter5, PFIL5_MAT_UUC,  &Filter5P->Mult[0][2]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_UC,   &Filter5P->Mult[1][2]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_CC,   &Filter5P->Mult[2][2]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_DC,   &Filter5P->Mult[3][2]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_DDC,  &Filter5P->Mult[4][2]);

			GetCtrlVal(PnlFilter5, PFIL5_MAT_UUR,  &Filter5P->Mult[0][3]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_UR,   &Filter5P->Mult[1][3]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_CR,   &Filter5P->Mult[2][3]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_DR,   &Filter5P->Mult[3][3]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_DDR,  &Filter5P->Mult[4][3]);

			GetCtrlVal(PnlFilter5, PFIL5_MAT_UURR, &Filter5P->Mult[0][4]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_URR,  &Filter5P->Mult[1][4]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_CRR,  &Filter5P->Mult[2][4]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_DRR,  &Filter5P->Mult[3][4]);
			GetCtrlVal(PnlFilter5, PFIL5_MAT_DDRR, &Filter5P->Mult[4][4]);

			QuitUserInterface(0);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Confirm Custom Filter values",
				"Apply the custom values of your filter.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Filter1COK (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(PnlFilterC, PFILC_NAME,  FilterCP->Name);

			GetCtrlVal(PnlFilterC, PFILC_MAT_RR,  &FilterCP->Mult[0][0]);
			GetCtrlVal(PnlFilterC, PFILC_MAT_RG,  &FilterCP->Mult[0][1]);
			GetCtrlVal(PnlFilterC, PFILC_MAT_RB,  &FilterCP->Mult[0][2]);
			GetCtrlVal(PnlFilterC, PFILC_MAT_RA,  &FilterCP->Mult[0][3]);
	
			GetCtrlVal(PnlFilterC, PFILC_MAT_GR,  &FilterCP->Mult[1][0]);
			GetCtrlVal(PnlFilterC, PFILC_MAT_GG,  &FilterCP->Mult[1][1]);
			GetCtrlVal(PnlFilterC, PFILC_MAT_GB,  &FilterCP->Mult[1][2]);
			GetCtrlVal(PnlFilterC, PFILC_MAT_GA,  &FilterCP->Mult[1][3]);

			GetCtrlVal(PnlFilterC, PFILC_MAT_BR,  &FilterCP->Mult[2][0]);
			GetCtrlVal(PnlFilterC, PFILC_MAT_BG,  &FilterCP->Mult[2][1]);
			GetCtrlVal(PnlFilterC, PFILC_MAT_BB,  &FilterCP->Mult[2][2]);
			GetCtrlVal(PnlFilterC, PFILC_MAT_BA,  &FilterCP->Mult[2][3]);

			GetCtrlVal(PnlFilterC, PFILC_MAT_AR,  &FilterCP->Mult[3][0]);
			GetCtrlVal(PnlFilterC, PFILC_MAT_AG,  &FilterCP->Mult[3][1]);
			GetCtrlVal(PnlFilterC, PFILC_MAT_AB,  &FilterCP->Mult[3][2]);
			GetCtrlVal(PnlFilterC, PFILC_MAT_AA,  &FilterCP->Mult[3][3]);

			GetCtrlVal(PnlFilterC, PFILC_DIV_R,  &FilterCP->Div[0]);
			GetCtrlVal(PnlFilterC, PFILC_DIV_G,  &FilterCP->Div[1]);
			GetCtrlVal(PnlFilterC, PFILC_DIV_B,  &FilterCP->Div[2]);
			GetCtrlVal(PnlFilterC, PFILC_DIV_A,  &FilterCP->Div[3]);

			GetCtrlVal(PnlFilterC, PFILC_BIAS_R, &FilterCP->Bias[0]);
			GetCtrlVal(PnlFilterC, PFILC_BIAS_G, &FilterCP->Bias[1]);
			GetCtrlVal(PnlFilterC, PFILC_BIAS_B, &FilterCP->Bias[2]);
			GetCtrlVal(PnlFilterC, PFILC_BIAS_A, &FilterCP->Bias[3]);

			QuitUserInterface(0);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Confirm Custom Filter values",
				"Apply the custom values of your filter.");
			break;
	}
	return 0;
}


int CVICALLBACK cb_FilterCancel (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			QuitUserInterface(0);
			break;
		case EVENT_RIGHT_CLICK:

			break;
	}
	return 0;
}

