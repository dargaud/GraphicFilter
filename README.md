More info here: http://www.gdargaud.net/Hack/SourceCode.html#GraphicFilter

There are 4 digital filters (ANSI C) and associated popups (for LabWindows/CVI):  
 - a pixel filter (Mult/div/bias)
 - a 3x3 matrix filter
 - a 5x5 matrix filter
 - a channel mixing filter (for color images)